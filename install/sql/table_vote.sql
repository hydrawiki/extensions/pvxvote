CREATE TABLE /*_*/vote (
  `vote_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vote_user` int(10) DEFAULT NULL,
  `vote_lottery` int(5) DEFAULT NULL,
  `vote_choice` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) /*$wgDBTableOptions*/;
