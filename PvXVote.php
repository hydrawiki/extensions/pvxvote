<?php
 /**
  * PvX Vote
  *
  * @author		Rob Church / gcardinal, Cameron Chunn
  * @package		PvXVote
  *
 **/
 if ( function_exists( 'wfLoadExtension' ) ) {
 	wfLoadExtension( 'PvXVote' );
 	// Keep i18n globals so mergeMessageFileList.php doesn't break
 	$wgMessagesDirs['PvXVote'] = __DIR__ . '/i18n';
 	wfWarn(
 		'Deprecated PHP entry point used for PvX Vote extension. Please use wfLoadExtension instead, ' .
 		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
 	);
 	return;
 } else {
 	die( 'This version of the PvX Vote extension requires MediaWiki 1.25+' );
 }
