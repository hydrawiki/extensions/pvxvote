<?php
/**
 * Curse Inc.
 * PvX Vote
 * Lottery added by gcardinal.  Modified for pvxwiki.
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2015 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		PvXVote
 * @link		https://gitlab.com/hydrawiki
 *
**/

class PvXVoteHooks {
	/**
	 * @param  DatabaseUpdater $updater
	 * @return true;
	 */
	public static function onLoadExtensionSchemaUpdates( DatabaseUpdater $updater ) {
		$updater->addExtensionUpdate( array( 'addTable', 'vote',
			__DIR__ . '/install/sql/table_vote.sql', true ) );
		return true;
	}
}
