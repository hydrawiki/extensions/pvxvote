<?php
/**
 * Curse Inc.
 * PvX Vote
 * Lottery added by gcardinal.	Modified for pvxwiki.
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2015 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		PvXVote
 * @link		https://gitlab.com/hydrawiki
 *
**/

class SpecialVote extends SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct(
			'Vote', // name
			'vote', // required user right
			true // display on Special:Specialpages
		);
		$this->output = $this->getOutput();
		$this->DB = wfGetDB(DB_MASTER);
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($mode = false) {
		global $wgUser;

		$this->output->addModules('ext.pvxvote');

		$this->setHeaders();
		$this->user = &$wgUser;
		if (strtolower($mode) == 'results') {
			if ($wgUser->isAllowed('voteadmin')) {
				$this->showResults();
			}
		} else {
			if (($wgUser->isAnon())) {
				$skin = $this->getSkin();
				$self = SpecialPage::getTitleFor('Vote');
				$login = SpecialPage::getTitleFor('Userlogin');
				$linkRenderer = $this->getLinkRenderer();
				$link = $linkRenderer->makeLink($login, wfMessage('vote-login-link')->escaped(), [],
					'returnto=' . $self->getPrefixedUrl());
				$this->output->addHtml(wfMessage('vote-login', $link)->parse());
				return;
			} elseif (!$wgUser->mEmailAuthenticated) {
				$this->output->addWikiText("'''Email authentication is required to vote.'''

 Please edit/add your e-mail using [[Special:Preferences]] and a confirmation e-mail will be sent to your e-mail address.

 Follow the instructions in the e-mail, to confirm that the account is actually yours.");
				return;
			} elseif (!$wgUser->isAllowed('vote')) {
				throw new PermissionsError('vote');
				return;
			} else {
				$this->showNormal();
			}
		}
	}

	/**
	 * Display Normal View
	 * @return void [Outputs to screen]
	 */
	private function showNormal() {
		global $wgRequest, $wgUser;
		$self = SpecialPage::getTitleFor('Vote');
		$token = $wgRequest->getText('token');
		if ($wgUser->isAllowed('voteadmin')) {
			$skin = $this->getSkin();
			$rtitle = Title::makeTitle(NS_SPECIAL, $self->getText() . '/results');
			$linkRenderer = $this->getLinkRenderer();
			$rlink = $linkRenderer->makeLink($rtitle, wfMessage('vote-view-results')->escaped());
			$this->output->addHtml('<p class="mw-voteresultslink">' . $rlink . '</p>');
		}
		$this->output->addHtml(wfMessage('vote-header')->parse());
		$current = self::getExistingVote($wgUser);
		if ($wgRequest->wasPosted() && $wgUser->matchEditToken($token, 'vote')) {
			$vote = strtolower($wgRequest->getText('vote'));
			if (in_array($vote, array_keys($this->getChoices()))) {
				self::updateVote($wgUser, $vote);
				$lottery = self::getLotteryNumber($wgUser);
				$this->output->addHtml('<p class="mw-votesuccess">' . wfMessage('vote-registered')->escaped() . '</p>');
				$this->output->addHtml('<p class="mw-voteerror">' . "Your lottery number: " . $lottery . '.</p>');
				$this->output->addWikiText('* You can change your vote anytime during the voting time.');
				$this->output->addWikiText('* Upon completion of voting, one user with the highest lottery number will receive a prize.');
				$this->output->addWikiText('* Any change of your vote will not change or affect your lottery number. ');
				$this->output->addWikiText('* Lottery numbers are generated randomly using a random number generator service at www.random.org.');
			} else {
				$this->output->addHtml('<p class="mw-voteerror">' . wfMessage('vote-invalid-choice')->escaped() . '</p>');
				$this->output->addHtml($this->makeForm($current));
			}
		} else {
			if ($current !== false) {
				$this->output->addWikiText(wfMessage('vote-current', $this->getChoiceDesc($current))->text());
				$lottery = self::getLotteryNumber($wgUser);
				$this->output->addWikiText("Your lottery number: '''" . $lottery . ".'''");
			}
			$this->output->addHtml($this->makeForm($current));
		}
	}

	/**
	 * Get exesting vote for user
	 * @param  user $user
	 * @return $choice	[The users current vote choice]
	 */
	private function getExistingVote(&$user) {
		$choice = $this->DB->selectField('vote', 'vote_choice', array('vote_user' => $user->getId()), __METHOD__);
		return $choice;
	}

	/**
	 * Get users lottery number
	 * @param  user $user
	 * @return $choice		 [The lottery number]
	 */
	private function getLotteryNumber(&$user) {
		$choice = $this->DB->selectField('vote', 'vote_lottery', array('vote_user' => $user->getId()), __METHOD__);
		return $choice;
	}

	/**
	 * Update vote for user
	 * @param	user $user
	 * @param  $choice [The choice voted on]
	 * @return void
	 */
	private function updateVote(&$user, $choice) {
		$this->DB->startAtomic(__METHOD__);
		$lottoNumber = $this->DB->selectField('vote', 'vote_lottery', array('vote_user' => $user->getId()), __METHOD__);
		if ($lottoNumber < 1) {
			$randNumber = Http::get('https://www.random.org/integers/?num=1&min=10000&max=19999&col=1&base=10&format=plain&rnd=new');
			if (!is_numeric($randNumber)) {
				srand((double)microtime() * 1000000);
				$randNumber = rand(10000, 19999);
			}
			$lottoNumber = $randNumber;
		}
		$this->DB->delete('vote', array('vote_user' => $user->getId()), __METHOD__);
		$this->DB->insert('vote', array('vote_user' => $user->getId(), 'vote_choice' => $choice,
			'vote_lottery' => $lottoNumber), __METHOD__);
		$this->DB->endAtomic(__METHOD__);
	}

	/**
	 * Show vote results
	 * @return [type] [description]
	 */
	private function showResults() {
		global $wgLang;
		$this->output->setPageTitle(wfMessage('vote-results')->plain());
		$vote = $this->DB->tableName('vote');
		//$res = $this->DB->query("SELECT vote_choice, COUNT(*) as count FROM {$vote} GROUP BY vote_choice ORDER BY count DESC", __METHOD__);
		$res = $this->DB->select(
			['vote'],
			[
				'vote_choice',
				'COUNT(*) as total'
			],
			null,
			__METHOD__,
			[
				'ORDER BY'	=> 'total DESC',
				'GROUP BY'	=> 'vote_choice',
			]
		);

		if ($res && $this->DB->numRows($res) > 0) {
			$this->output->addHtml('<table class="mw-votedata"><tr>');
			$this->output->addHtml('<th>' . wfMessage('vote-results-choice')->escaped() . '</th>');
			$this->output->addHtml('<th>' . wfMessage('vote-results-count')->escaped() . '</th></tr>');
			while ($row = $this->DB->fetchObject($res)) {
				$this->output->addHtml('<tr><td>' . htmlspecialchars($this->getChoiceDesc($row->
					vote_choice)) . '</td>');
				$this->output->addHtml('<td>' . $wgLang->formatNum($row->total) . '</td></tr>');
			}
			$this->output->addHtml('</table>');
		} else {
			$this->output->addWikiText(wfMessage('vote-results-none')->plain());
		}
	}

	/**
	 * Get vote choices
	 * @return array
	 */
	private function getChoices() {
		static $return = false;
		if (!$return) {
			$return = array();
			$lines = explode("\n", wfMessage('vote-choices')->plain());
			foreach ($lines as $line) {
				list($short, $long) = explode('|', $line, 2);
				$return[strtolower($short)] = $long;
			}
		}
		return $return;
	}

	/**
	 * Get description for choice
	 * @param  str $short [Choice name]
	 * @return str		 [The description]
	 */
	private function getChoiceDesc($short) {
		$choices = $this->getChoices();
		return $choices[$short];
	}

	/**
	 * Make the vote form
	 * @param  $current
	 * @return string	Return the HTML of the form
	 */
	private function makeForm($current) {
		global $wgUser;

		$self = $this->getTitle();
		$form = Xml::openElement('form', array('method' => 'post', 'action' => $self->
			getLocalUrl()));
		$form .= Xml::input('token', 40, $wgUser->getEditToken('vote'), ['type' => 'hidden']);
		$form .= '<fieldset><legend>' . wfMessage('vote-legend')->escaped() . '</legend>';
		$form .= '<p>' . Xml::label(wfMessage('vote-caption')->plain(), 'vote') . '&nbsp;';
		$form .= Xml::openElement('select', array('name' => 'vote', 'id' => 'vote'));
		foreach ($this->getChoices() as $short => $desc) {
			$checked = $short == $current;
			$form .= self::makeSelectOption($short, $desc, $checked);
		}
		$form .= Xml::closeElement('select');
		$form .= '&nbsp;' . Xml::submitButton(wfMessage('vote-submit')->plain());
		$form .= '</fieldset></form>';
		return $form;
	}

	/**
	 * Returns an <option> tag
	 * @param  var $value
	 * @param  var $label
	 * @param  var $selected
	 * @return Xml
	 */
	private static function makeSelectOption($value, $label, $selected = false) {
		$attribs = array();
		$attribs['value'] = $value;
		if ($selected) {
			$attribs['selected'] = 'selected';
		}
		return Xml::openElement('option', $attribs) . htmlspecialchars($label) . Xml::
			closeElement('option');
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access protected
	 * @return string
	 */
	protected function getGroupName() {
		return 'pvx'; //Change to display in a different category on Special:SpecialPages.
	}
}
